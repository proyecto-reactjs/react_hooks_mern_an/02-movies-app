# Curso React JS Hooks: De Cero a Experto Creado Aplicaciones Reales

## Proyecto Movies App

Instruido por:

Agustin Navarro Galdon

(https://www.youtube.com/watch?v=LnPEKBgs2HA)

Puede ver el proyecto aquí: (https://proyecto-reactjs.gitlab.io/react_hooks_mern_an/02-movies-app)

[![MoviesApp](https://i.imgur.com/zBQIWdv.png)](https://www.youtube.com/watch?v=LnPEKBgs2HA)

## Nota:

creado con:

`npx create-react-app`

### Comandos:

- `npm install query-string`
- `npm install react-router-dom`
- `npm install --save-dev sass`
- `npm install antd`
- `npm install react-player`
- `npm i rc-pagination`
- `npm install --save @ant-design/icons`

### Para usar Gitlab pages:

(https://proyecto-reactjs.gitlab.io/react_hooks_mern_an/02-movies-app)

- #### Basename que esta en AppRouter:

  - `<Router basename= "/react_hooks_mern_an/02-movies-app/" >`
  - `/react_hooks_mern_an/02-movies-app/` reemplazarlo por tu Url base

####

- #### Colocarlo en el package.json:

  - `"homepage": "https://proyecto-reactjs.gitlab.io/react_hooks_mern_an/02-movies-app/"`,
  - `/react_hooks_mern_an/02-movies-app/` reemplazarlo por tu Url base

####

- #### Esta documentación me ayudo:

  (https://create-react-app.dev/docs/deployment/#building-for-relative-paths)
